﻿using Application.Dto;
using Domain.Common.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Services
{
    public interface IUserServices
    {
        Task<Response<UserDto>> CreateUser(CreateUserDto createUserDto);

        Task<Response<UserDto>> GetUserByNameAsync(string userName);
    }
}
